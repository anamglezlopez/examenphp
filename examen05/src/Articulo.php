<?php

namespace src;

class Articulo
{
    private ?string $nombre;
    private ?float $precio;
    

    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li>Nombre: " . $this->nombre . "</li><br>";
        $salida .= "<li>Precio: " . $this->precio ."€" . "</li>";
        $salida .= "</ul>";
        return $salida;
    }

    // creo el metodo magico toString
    public function __toString()
    {
        return $this->mostrar();
    }

    public function __construct()
    {
        $this->nombre = null;
        $this->precio = 0;
        
    }


    /**
     * Get the value of nombre
     *
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @param string $nombre
     *
     * @return self
     */
    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of precio
     *
     * @return float
     */
    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @param int $precio
     *
     * @return self
     */
    public function setPrecio(?float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

}
