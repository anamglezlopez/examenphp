<?php

namespace src;

class ArticuloRebajado extends Articulo
{
    protected ?float $rebaja;

    // sobreescribir mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li> Nombre: " . $this->getNombre() . "</li>";
        $salida .= "<li> Precio: " . $this->getPrecio() . "</li>";
        $salida .= "<li> Rebaja: " . $this->rebaja . "</li>";
        $salida .= "</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent::__construct();
        $this->rebaja = null;
        
    }

    public function calculaDescuento():float
    {
        $descuento=($this->getPrecio()*$this->rebaja)/100;

        return $descuento;  
    }
    public function precioRebajado():float
    {
        $rebajado= $this->getPrecio()-$this->calculaDescuento();

        return $rebajado;  
    }

  

    // creo el metodo magico toString
    public function __toString()
    {
        parent::__toString();
        
        // asigno los campos que no estan en el padre
        $salida = "La rebaja es: ";
        $salida .= $this->rebaja . "%" ;
        $salida .= "Y el descuento es: ";
        $salida .= $this->calculaDescuento(). " €";
        return $salida;

        
    }

}
