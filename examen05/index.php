<?php
// carga la clase empleado en el espacio de nombres actual
use src\Articulo;

use src\ArticuloRebajado;

// para poder utilizar sesiones
session_start();

// para carga automatica de clases
require_once "autoload.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba</title>
</head>

<body>
    <?php
    // cargar el formulario de articulo
    require "_articulo.php";
    // cargar el formulario de articuloREbajado
    require "_articuloRebajado.php";
    
    ?>
   
    <hr>
    <h2>ARTICULOS</h2>
    <?php
    if (isset($_SESSION["articulo"])) {
        foreach ($_SESSION["articulos"] as $articulo) {
            $articulo = unserialize($articulo); 
            echo $articulo; // llamo al metodo toString
        }
    } else {
        echo "No hay Articulos";
    }

    ?>
    <hr>
    <h2>ARTICULOS REBAJADOS</h2>
    <?php
    if (isset($_SESSION["articulorebajado"])) {
        foreach ($_SESSION["articulosRebajados"] as $articuloRebajado) {
            $articulosRebajados = unserialize($articuloRebajado); 
            echo $articulosRebajados; // llamo al metodo toString
        }
    } else {
        echo "No hay Articulos";
    }
?>
</body>

</html>