DROP DATABASE IF EXISTS pruebaPhp;
CREATE DATABASE pruebaPhp;
USE pruebaPhp;

CREATE TABLE autores(
id int AUTO_INCREMENT,
nombre varchar(200),
apellidos varchar(200),
PRIMARY KEY(id)
);

INSERT INTO autores(nombre,apellidos)
             VALUES ('Francisco','de Goya');