<?php

namespace clases;

use mysqli;
use mysqli_result;

class Modelo
{
    private mysqli $db;
    private mysqli_result $resultados;

    public function __construct(mysqli $db)
    {
        $this->db = $db;
    }

    public function query($consulta): mysqli_result
    {
        $this->resultados = $this->db->query($consulta);
        return $this->resultados;
    }
    public function gridViewBotones(array $botones = [], array $campos = []): string
    {
        if ($this->resultados->num_rows > 0) {
            $registros = $this->resultados->fetch_all(MYSQLI_ASSOC);
            // mostrar los registros
            $salida = "<table class='table table-striped table-bordered'>";
            $salida .= "<thead class='table-dark'><tr>";

            // compruebo si el usuario me ha pasado los campos a mostrar
            if (count($campos) == 0) {
                // si no me ha pasado los campos los leo del primer registro de la base de datos
                $campos = array_keys($registros[0]);
            }

            foreach ($campos as $campo) {
                $salida .= "<th>$campo</th>";
            }
            if (count($botones) > 0) {
                // añado una columna para los botones
                $salida .= "<td>Acciones</td>";
            }

            $salida .= "</tr></thead>";
            // muestro todos los registros
            foreach ($registros as $registro) {
                $salida .= "<tr>";
                // mostrando los campos que hayamos seleccionado
                foreach ($campos as $campo) {
                    $salida .= "<td>" . $registro[$campo] . "</td>";
                }
                // mostrando los botones
                if (count($botones) > 0) {
                    $salida .= "<td>";

                    foreach ($botones as $label => $enlace) {
                        $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> ";
                    }

                    // cerramos la celda
                    $salida .= "</td>";
                }

                $salida .= "</tr>";
            }
            $salida .= "</table>";
        } else {
            $salida = "No hay registros";
        }
        // colocar el puntero al principio de los registros
        $this->resultados->data_seek(0);

        return $salida;
    }
}
