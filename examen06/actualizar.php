<?php
session_start();

require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");
$tabla = "libros";
$accion = "actualizar";

// desactivar errores
controlErrores();


// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
$menu = menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// comprobar si vengo de pulsar sobre el boton editar
// del gridview y por lo tanto me llega el id por GET
if (isset($_GET["id"])) {

    $_SESSION["id"] = $_GET["id"];
    // preparo la consulta para obtener los datos del libro 
    // a modificar
    $sql = "select * from {$tabla} where id={$_GET["id"]}";
    $resultado = $conexion->query($sql);
    $datos = $resultado->fetch_assoc();
}

// comprobar si he pulsado de actualizar en el formulario
// despues de que el usuario haya cambiado los datos
if ($_POST) {
    foreach ($_POST as $campo => $valor) {
        $datos[$campo] = $valor;
    }
    // update libros set titulo='', paginas='', fechaPublicacion='' where id=1

    $sql = "UPDATE {$tabla} SET titulo='{$datos['titulo']}', paginas={$datos['paginas']}, fechaPublicacion='{$datos['fechaPublicacion']}' WHERE id={$_SESSION['id']}";

    if ($resultado = $conexion->query($sql)) {

        $sql = "select * from {$tabla} where id={$_SESSION['id']}";

        $resultado = $conexion->query($sql);
        $salida = gridView($resultado);
        $salida .= "<div class='alert alert-success'>Registro actualizado correctamente</div>";
    } else {
        $salida = "Error al actualizar el registro" . $conexion->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <div class="container">

        <div class="row">
            <div class="bg-dark text-light">
                <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Actualizar </h1>
            </div>
        </div>
        <br><br>
        <div class="row">
            <?= $menu ?>
        </div>
        <br><br>
        <div class="row">
            <?php
            if (!$_POST) {
                require "_form.php";
            }
            ?>
        </div>
        <br><br>
        <div class="row">
            <?= $salida ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
