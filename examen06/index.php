<?php

// cargando la clase en el espacio de nombres actual
use clases\Autores;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// instanciando la clase
$autores = new Autores();
$autor = new Modelo($autores->db);
$query = $autor->query("select * from autores");
$cabecera = Header::ejecutar([
    "titulo" => "Pagina de inicio",
    "subtitulo" => 'PRUEBA PHP',
    "salida" => "Aplicacion para mostrar paginas interesantes"
]);

Pagina::comenzar();
?>

<?= GridView::render($query, [
    "ver" => "ver.php"
], [
    "id", "titulo", "url"
]) ?>

<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
