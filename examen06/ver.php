<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\DetailView;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// creo un objeto de tipo aplicacion
$aplicacion = new Aplicacion();
// creo un objeto de tipo mysqli 
// es una conexion con la bbdd
$favoritos = new Modelo($aplicacion->db);
$_GET["id"] = $_GET["id"] ?? 0;
$query = $favoritos->query("select * from favoritos where id = " . $_GET['id']);

$cabecera = Header::ejecutar([
    "titulo" => "Detalles de la pagina",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => $_GET["id"]
]);


Pagina::comenzar();
if ($query->num_rows == 0) {
    DetailView::comenzar();
?>
    <li class="list-group-item"> No tenemos ningun registro</li>
<?php
    DetailView::terminar(["titulo" => "Aviso"]);
} else {
    echo DetailView::ejecutar([
        "query" => $query,
        "campos" => [
            "id",
            "url",
        ],
        "campoTitulo" =>  'titulo'

    ]);
}
?>

<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
