<?php
require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");
$tabla = "libros";
$accion = "insertar";

// desactivar errores
controlErrores();


// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
$menu = menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// creo el array de datos vacio para mandar al formulario
// en caso de que no haya pulsado el boton de insertar
$datos = [
    "titulo" => "",
    "paginas" => 0,
    "fechaPublicacion" => ""
];

// tengo que comprobar si vengo o no de pulsar el boton insertar en el formulario

if ($_POST) {
    // si he pulsado el boton leo los datos que envio desde el formulario

    foreach ($datos as $clave => $valor) {
        $datos[$clave] = $_POST[$clave];
    }

    // foreach($_POST as $clave => $valor){
    //     $datos[$clave] = $valor;
    // }

    // tengo que insertar el registro en la bbdd
    $query = "INSERT INTO {$tabla}(titulo,paginas,fechaPublicacion) VALUES ('{$datos["titulo"]}',{$datos["paginas"]},'{$datos["fechaPublicacion"]}')";

    // ejecuto la consulta
    if ($conexion->query($query)) {
        $salida = "<div class='alert alert-success'>Registro insertado</div>";
        // obtengo los datos del ultimo registro insertado
        $sql = "select * from libros where id={$conexion->insert_id}";

        $resultado = $conexion->query($sql);
        $salida .= gridView($resultado);
    } else {
        $salida = "Error insertando el registro" . $conexion->error;
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="bg-dark text-light">
                <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Insertar </h1>
            </div>
        </div>
        <br>
        <div class="row">
            <?= $menu ?>
        </div>
        <br>
        <div class="row">
            <?php
            if (!$_POST) {
                require "_form.php";
            }
            ?>
        </div>
        <br>
        <div class="row">
            <?= $salida ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
