DROP DATABASE IF EXISTS biblioteca;
CREATE DATABASE biblioteca;
USE biblioteca;

CREATE TABLE libros(
id int AUTO_INCREMENT,
titulo varchar(200),
paginas int,
fechaPublicacion date,
PRIMARY KEY(id)
);

INSERT INTO libros(titulo,paginas,fechaPublicacion)
             VALUES ('Marianela',226,'1985-06-29');