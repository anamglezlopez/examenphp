<form method="post">
    <div class="row">
        <label class="col-sm-2 col-form-label" for="titulo">Titulo</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="titulo" value="<?= $datos['titulo'] ?>">
        </div>
    </div>
    <br><br>
    <div class="row">
        <label for="paginas" class="col-sm-2 col-form-label">Paginas</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" name="paginas" value="<?= $datos['paginas'] ?>">
        </div>
    </div>
    <br><br>
    <div class="row">
        <label for="fechaPublicacion" class="col-sm-2 col-form-label">fechaPublicacion</label>
        <div class="col-sm-10">
            <input type="date" class="form-control" name="fechaPublicacion" value="<?= $datos['fechaPublicacion'] ?>">
        </div>
    </div>
    <br><br>
    <div>
        <button class="btn btn-primary"><?= $accion ?></button>
        <button class="btn btn-secondary" type="reset">Limpiar</button>
    </div>
</form>
