<?php

require_once "funciones.php";

$tabla = "libros";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();

// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
$menu = menu($elementosMenu);

// necesito los libros

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// preparo la consulta
$sql = "select * from {$tabla}";

// creo un objeto de tipo mysqli_result de la consulta
// y ademas compruebo si la consulta es correcta
if ($resultado = $conexion->query($sql)) {
    // crear un string con la tabla de todos los registros
    $salida = gridViewBotones($resultado, [
        '<i class="fa-solid fa-pen-to-square"></i> Editar' => "actualizar.php",
        '<i class="fa-solid fa-trash"></i> Eliminar' => "eliminar.php"
    ]);
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="bg-dark text-light">
                <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Inicio</h1>
            </div>
        </div>
        <br>
        <div class="row">
            <?= $menu ?>
        </div>
        <br>
        <div class="row">
            <?= $salida ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
