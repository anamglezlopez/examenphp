<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css" >
    <title>EJERCICIO 001</title>
</head>
<body>
    <h3>EJERCICIO 1 DEL EXAMEN DE PHP</h3>

    <p class="cabecera">Escriba un numero (ENTRE 0 Y 10) y dibujare una tabla de una columna de ese tamaño con cajs ade texto en cada celda</p>
    <br>
    <div class="contenedor">
        <div class="titulo">
            EJERCICIO 1
        </div>

        <div class="contenido">
            <form method="post" action="./tabla.php">
                <label>Tamaño de la tabla: *</label>
                <input type="text" id="numero" name="numero" required="required"> Introduce un numero</input>
                <p>Campo requerido</p>
                <div class="botones">
                    <button type="submit">Dibujar</button>
                    <button type="reset">Restablecer</button>
                </div>
            </form>
        </div>
    </div>
    <br>
    <hr>
    <br>
    <div>
        <p>Esta pagina forma parte del curso POO2023</p>
        <p>ANA MARIA GONZÁLEZ LÓPEZ</p>
    </div>


</body>
</html>